<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Pedido;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('TCG\Voyager\Models\Post', function ($app) {
            return new App\Post;
        });
    }
}
